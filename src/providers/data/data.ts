import { Response, Http } from '@angular/http'
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

/*
  Generated class for the DataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DataProvider {

  data: any;
  constructor(public _http: Http) {
    console.log('Hello DataProvider Provider');
  }

  getData (url) {
    return this._http.get(url)
      .map((res: Response) => res.json());
  }

  load(factual_id) {
    // don't have the data yet
    return new Promise(resolve => {
     
      // We're using Angular HTTP provider to request the data,
      // then on the response, it'll map the JSON data to a parsed JS object.
      // Next, we process the data and resolve the promise with the new data.
      this._http.get('https://accessearth-dev.azurewebsites.net/php/ae_profiles.php?method=fetchPlaceMobile&place_id='+factual_id)
        .map(res => res.json())
        .subscribe(data => {
          // we've got back the raw data, now generate the core schedule data
          // and save the data for later reference
          this.data = data;
          resolve(this.data);
        });
    });
    }

}
