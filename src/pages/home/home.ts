import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, ModalController, FabContainer, ActionSheetController, Platform } from 'ionic-angular';
import leaflet from 'leaflet';
import { DataProvider } from '../../providers/data/data';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  @ViewChild('fab') fab: FabContainer;
  @ViewChild('map') mapContainer: ElementRef;
  categorySlot: any;
  iconValue: any;
  icon: any;
  point: any;
  popup: any;
  html: any;

  iconLink: any;
  displayedMarkers: any;
  categories = JSON.parse('[{"id":"1","name":"Place to sleep","iconClass":"fa-bed","mapMarkerUrl":"https://accessearth-dev.azurewebsites.net/app/assets/pins/sleep-pin"},{"id":"2","name":"Place to eat/drink","iconClass":"fa-cutlery","mapMarkerUrl":"https://accessearth-dev.azurewebsites.net/app/assets/pins/eat-pin"},{"id":"3","name":"Somewhere to shop","iconClass":"fa-shopping-bag","mapMarkerUrl":"https://accessearth-dev.azurewebsites.net/app/assets/pins/shop-pin"},{"id":"4","name":"Something to do","iconClass":"fa-camera","mapMarkerUrl":"https://accessearth-dev.azurewebsites.net/app/assets/pins/do-pin"},{"id":"5","name":"Education","iconClass":"fa-graduation-cap","mapMarkerUrl":"https://accessearth-dev.azurewebsites.net/app/assets/pins/education-pin"},{"id":"6","name":"Utilities","iconClass":"fa-university","mapMarkerUrl":"https://accessearth-dev.azurewebsites.net/app/assets/pins/utility-pin"},{"id":"7","name":"How to get there","iconClass":"fa-bus","mapMarkerUrl":"https://accessearth-dev.azurewebsites.net/app/assets/pins/transport-pin"}]');

  customIconMarker = leaflet.Icon.extend({
    options: {
      id: 0,
      name: 'data',
      category: 0,
      address: 'data',
      lat: 0.1,
      lon: 0.1,
      rating: 0,
      step_free: 0,
      doors_wide: 0,
      primary: 0,
      toilets: 0,
      unrated: 0
    }
  });

  allmarkers: any;
  map: any;
  places: any;

  private all_url = 'assets/avivaData.json';

  wheelchair = {
    ext_parking_available: "1",
    ext_step_free: "1",
    ext_doors_wide: "0",
    int_primary_functions: "1",
    int_rest_wheelchair: "1",
    int_rest_changing: "1",
    int_hotel_room: "1",
    int_hotel_shower: "1",
  };
  electric = {
    ext_parking_available: "1",
    ext_step_free: "1",
    ext_doors_wide: "1",
    int_primary_functions: "1",
    int_rest_wheelchair: "1",
    int_rest_changing: "1",
    int_hotel_room: "1",
    int_hotel_shower: "1",
  };
  walking = {
    ext_parking_available: "0",
    ext_step_free: "1",
    ext_doors_wide: "0",
    int_primary_functions: "1",
    int_rest_wheelchair: "0",
    int_rest_changing: "1",
  };
  rollator = {
    ext_parking_available: "1",
    ext_step_free: "1",
    ext_doors_wide: "1",
    int_primary_functions: "1",
    int_rest_wheelchair: "1",
    int_rest_changing: "1",
  }

  wheelArray = [];
  electricArray = [];
  walkArray = [];
  rollArray = [];

  electricA = false;
  wheelA = false;
  walkingA = false;
  rollatorA = false;

  bedA = false;
  foodA = false;
  shoppingA = false;
  cameraA = false;

  element: any;
  actionSheetButtons = [
    {
      text: 'Place Details',
      icon: !this.platform.is('ios') ? 'md-help-circle' : null,
      handler: () => {
        this.goToDetail(this.element);
      }
    },
    {
      text: 'Cancel',
      role: 'cancel', // will always sort to be on the bottom
      icon: !this.platform.is('ios') ? 'close' : null,
      handler: () => {
        this.map.setView(leaflet.latLng(53.335231800, -6.228456900), 16);
        this.map.closePopup();
      }
    }
  ];
  isOpened = false;

  constructor(public navCtrl: NavController, public dataprovider: DataProvider, public modalCtrl: ModalController, public actionsheetCtrl: ActionSheetController, public platform: Platform) {

  }

  ionViewDidEnter() {
    this.fab.toggleList();
    this.loadmap();
    this.getData(this.all_url);
  }

  getData(url) {
    this.places = [];
    this.dataprovider.getData(url).subscribe((json) => {
      this.places = json;
      this.assignData();
    });
  }

  filter(device) {
    this.map.removeLayer(this.displayedMarkers);
    this.electricA = false;
    this.wheelA = false;
    this.walkingA = false;
    this.rollatorA = false;
    if (device == 'walking') {
      this.walkingA = true;
      this.loadMarkers(this.walkArray);
    }
    if (device == 'rollerator') {
      this.rollatorA = true;
      this.loadMarkers(this.rollArray);
    }
    if (device == 'wheelchair') {
      this.wheelA = true;
      this.loadMarkers(this.wheelArray);
    }
    if (device == 'electric') {
      this.electricA = true;
      this.loadMarkers(this.electricArray);
    }
  }

  filterCategories(category_id) {
    this.bedA = false;
    this.foodA = false;
    this.shoppingA = false;
    this.cameraA = false;

    if (category_id == 1) {
      this.bedA = true;
    }
    if (category_id == 2) {
      this.foodA = true;
    }
    if (category_id == 3) {
      this.shoppingA = true;
    }
    if (category_id == 4) {
      this.cameraA = true;
    }
    this.map.removeLayer(this.displayedMarkers);
    let result = this.places.filter(obj => {
      return obj.category_id == category_id;
    });
    this.loadMarkers(result);
  }

  fabToggled() {
    this.isOpened = !this.isOpened;

    if (!this.isOpened) {
      this.map.removeLayer(this.displayedMarkers);
      this.loadMarkers(this.places);
      this.bedA = false;
      this.foodA = false;
      this.shoppingA = false;
      this.cameraA = false;
      this.electricA = false;
      this.wheelA = false;
      this.walkingA = false;
      this.rollatorA = false;
    }
  }

  loadmap() {
    this.map = leaflet.map("map", {
      center: [49.85866436927149, 16.4916595422228056],
      zoom: 4,
      minZoom: 4
    });
    leaflet.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attributions: 'www.tphangout.com'
    }).addTo(this.map);

    let markerGroup = leaflet.featureGroup();

    let
      marker1: any = leaflet.marker([53.335231800, -6.228456900], { title: "Aviva" }).on('click', () => {
        this.map.setView(leaflet.latLng(53.335231800, -6.228456900), 16);
        this.loadMarkers(this.places);
      }),
      marker2: any = leaflet.marker([51.555998, -0.279544], { title: "Wembley" }).on('click', () => {
        this.map.setView(leaflet.latLng(51.555998, -0.279544), 15);
      }),
      marker3: any = leaflet.marker([41.934052, 12.454720], { title: "Stadio Olimpico" }).on('click', () => {
        this.map.setView(leaflet.latLng(41.934052, 12.454720), 15);
      }),
      marker4: any = leaflet.marker([48.218800, 11.624707], { title: "Allianz Stadium" }).on('click', () => {
        this.map.setView(leaflet.latLng(48.218800, 11.624707), 15);
      }),
      marker5: any = leaflet.marker([40.429958, 49.919602], { title: "Baku Olympic Stadium" }).on('click', () => {
        this.map.setView(leaflet.latLng(40.429958, 49.919602), 15);
      }),
      marker6: any = leaflet.marker([59.972728, 30.221405], { title: "St. Petersburg Stadium" }).on('click', () => {
        this.map.setView(leaflet.latLng(59.972728, 30.221405), 15);
      }),
      marker7: any = leaflet.marker([47.503172, 19.097022], { title: "Puskás Ferenc Stadium" }).on('click', () => {
        this.map.setView(leaflet.latLng(47.503172, 19.097022), 15);
      }),
      marker8: any = leaflet.marker([44.437139, 26.152579], { title: "National Arena" }).on('click', () => {
        this.map.setView(leaflet.latLng(44.437139, 26.152579), 15);
      }),
      marker9: any = leaflet.marker([52.314371, 4.941964], { title: "Amsterdam ArenA" }).on('click', () => {
        this.map.setView(leaflet.latLng(52.314371, 4.941964), 15);
      }),
      marker10: any = leaflet.marker([43.264135, -2.949365], { title: "San Mamés Zelaia" }).on('click', () => {
        this.map.setView(leaflet.latLng(43.264135, -2.949365), 15);
      }),
      marker11: any = leaflet.marker([55.825842, -4.252048], { title: "Hampden Par" }).on('click', () => {
        this.map.setView(leaflet.latLng(55.825842, -4.252048), 15);
      }),
      marker12: any = leaflet.marker([55.702299, 12.571556], { title: "Telia Parken" }).on('click', () => {
        this.map.setView(leaflet.latLng(55.702299, 12.571556), 15);
      });

    let stadiums = leaflet.layerGroup([
      marker1, marker2, marker3, marker4, marker5, marker6,
      marker7, marker8, marker9, marker10, marker11, marker12
    ]);

    markerGroup.addLayer(stadiums);
    this.map.addLayer(markerGroup);
  }

  goToDetail(place) {
    let modal = this.modalCtrl.create('DetailPage', { place: place }, { cssClass: 'modal-left' });
    modal.onDidDismiss(() => {
      this.map.setView(leaflet.latLng(53.335231800, -6.228456900), 16);
      this.map.closePopup();
    });
    modal.present();
  }

  loadMarkers(array) {
    this.allmarkers = [];
    array.forEach(value => {

      if (value.category_id < 0 || value.category_id > 6 || value.category_id == 'undefined') {
        this.categorySlot = 0;
      } else {
        this.categorySlot = value.category_id - 1;
      }

      this.iconValue = 'off.png';
      if (value.rated == 1) {
        this.iconValue = 'on.png';
      }

      this.iconLink = this.categories[this.categorySlot]['mapMarkerUrl'] + '-' + this.iconValue;
      this.icon = new this.customIconMarker({
        iconUrl: this.iconLink,
        iconSize: 50,
        place_name: value.name,
        factual_id: value.factual_id,
        category: value.category_id,
        lat: value.location.lat,
        lon: value.location.lng,
        step_free: value.ext_step_free,
        doors_wide: value.ext_doors_wide,
        primary: value.int_primary_functions,
        rest_available: value.int_rest_available,
        toilets: value.int_rest_wheelchair,
        rest_change: value.int_rest_change,
        address: value.address

      });
      this.point = leaflet.latLng(
        parseFloat(value.location.lat),
        parseFloat(value.location.lng)
      );
      this.html = leaflet.DomUtil.create('div', 'content');

      if (value.address == undefined) {
        value.address = '';
      }

      this.html.innerHTML = '<h4>' + value.name + '</h4><h5>' + value.categories[0].name + '</h5>';

      this.html.addEventListener('click', (e) => {
        this.openMenu(value);
      });
      this.popup = leaflet.popup().setContent(this.html);

      let marker = leaflet.marker(this.point, { icon: this.icon }).bindPopup(this.popup);

      marker.on('mouseover', () => {
        marker.openPopup();
      })

      marker.on('click', () => {
        marker.openPopup();
        this.openMenu(value);
      })

      this.allmarkers.push(marker);/**/
    });

    this.displayedMarkers = leaflet.layerGroup(this.allmarkers);
    this.map.addLayer(this.displayedMarkers);
  }

  openMenu(value) {
    this.map.setView(leaflet.latLng(value.location.lat, value.location.lng), 18);
    this.element = value;

    let address_place = value.location.address;
    if (value.location.city) {
      address_place = value.location.address + ", " + value.location.city;
    }

    let actionSheet = this.actionsheetCtrl.create({
      title: address_place,
      cssClass: 'action-sheets-basic-page',
      buttons: this.actionSheetButtons
    });
    actionSheet.present();
  }

  assignData() {

    this.places.forEach((value, i) => {
      if (i < 59) {
        value.rated = 1;
        value.ext_parking_available = this.electric.ext_parking_available;
        value.ext_step_free = this.electric.ext_step_free;
        value.ext_doors_wide = this.electric.ext_doors_wide;
        value.int_primary_functions = this.electric.int_primary_functions;
        value.int_rest_wheelchair = this.electric.int_rest_wheelchair;
        value.int_rest_changing = this.electric.int_rest_changing;
        value.int_hotel_room = this.electric.int_hotel_room;
        value.int_hotel_shower = this.electric.int_hotel_shower;

        this.electricArray.push(value);
      }
      else if (i >= 59 && i < 78) {
        value.rated = 1;
        value.ext_parking_available = this.wheelchair.ext_parking_available;
        value.ext_step_free = this.wheelchair.ext_step_free;
        value.ext_doors_wide = this.wheelchair.ext_doors_wide;
        value.int_primary_functions = this.wheelchair.int_primary_functions;
        value.int_rest_wheelchair = this.wheelchair.int_rest_wheelchair;
        value.int_rest_changing = this.wheelchair.int_rest_changing;
        value.int_hotel_room = this.wheelchair.int_hotel_room;
        value.int_hotel_shower = this.wheelchair.int_hotel_shower;

        this.wheelArray.push(value);
      }
      else if (i >= 98 && i < 117) {
        value.rated = 1;
        value.ext_parking_available = this.rollator.ext_parking_available;
        value.ext_step_free = this.rollator.ext_step_free;
        value.ext_doors_wide = this.rollator.ext_doors_wide;
        value.int_primary_functions = this.rollator.int_primary_functions;
        value.int_rest_wheelchair = this.rollator.int_rest_wheelchair;
        value.int_rest_changing = this.rollator.int_rest_changing;

        this.rollArray.push(value);
      }
      else {
        value.rated = 1;
        value.ext_parking_available = this.walking.ext_parking_available;
        value.ext_step_free = this.walking.ext_step_free;
        value.ext_doors_wide = this.walking.ext_doors_wide;
        value.int_primary_functions = this.walking.int_primary_functions;
        value.int_rest_wheelchair = this.walking.int_rest_wheelchair;
        value.int_rest_changing = this.walking.int_rest_changing;

        this.walkArray.push(value);
      }
    });

    this.walkArray = this.walkArray.concat(this.wheelArray, this.electricArray, this.rollArray);
    this.rollArray = this.rollArray.concat(this.wheelArray, this.electricArray);
    this.wheelArray = this.wheelArray.concat(this.electricArray);

  }

}