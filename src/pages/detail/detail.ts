import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, PopoverController, ViewController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';

@IonicPage()
@Component({
  selector: 'page-detail',
  templateUrl: 'detail.html',
})
export class DetailPage {

  hours_display = [];
  lng: any;
  lat: any;
  place: any;
  placeDetails: any;
  placeCriteria: any;
  primarydesc: any;
  cafeBlue = "#008DDF";

  constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform, public dataprovider: DataProvider, public popoverCtrl: PopoverController, public viewCtrl: ViewController) {
    this.place = navParams.get('place');
    this.lat = this.place['latitude'];
    this.lng = this.place['longitude'];
    this.loadPlaceDetails();

    platform.ready().then(() => {
      if (this.place['hours_display'] != undefined || this.place['hours_display'] != null) {
        this.hours_display = this.place['hours_display'].split(';');
      }
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailPage');
  }

  close() {
    this.viewCtrl.dismiss();
  }

  presentPopover(ev, place) {
    let popover = this.popoverCtrl.create("HelpPage", { place: place }, { cssClass: 'custom-popover' });
    popover.present({ ev: ev });
  }

  loadPlaceDetails() {
    let poi_id = this.place['factual_id'];
    let local = [
      {
        "name": "Accessible Parking",
        "value": this.place['ext_parking_available'],
        "id": "8",
        "display": true,
        "description": "Are wheelchair parking spots available at the venue? e.g. via private or on-street parking."
      },
      {
        "name": "Step Free",
        "value": this.place['ext_step_free'],
        "id": "1",
        "display": true,
        "description": "Is the entrance either on level ground or is there a step free route available? e.g. ramp or stair lift."
      },
      {
        "name": "Wide Doors",
        "value": this.place['ext_doors_wide'],
        "id": "2",
        "display": true,
        "description": "Is the door at least 32 inches wide?"
      },
      {
        "name": "Primary Functions",
        "value": this.place['int_primary_functions'],
        "id": "3",
        "display": true,
        "description": this.primarydesc
      },
      {
        "name": "Bathrooms Available",
        "value": this.place['int_rest_available'],
        "id": "4",
        "display": false,
        "description": "Are restrooms provided for the public?",
      },
      {
        "name": "Accessible Bathrooms",
        "value": this.place['int_rest_wheelchair'],
        "id": "5",
        "display": true,
        "description": "Does the venue show the wheelchair icon on the bathroom doors where they can be accessed via a step free route?",

      },
      {
        "name": "Changing Areas",
        "value": this.place['int_rest_changing'],
        "id": "9",
        "display": true,
        "description": "Changing Spaces are family restrooms that include adult-sized changing tables so persons with incontinence can be toileted."
      },
      {
        "name": "In-room Access",
        "value": this.place['int_hotel_room'],
        "id": "6",
        "display": false,
        "description": "Does the hotel provide rooms specifically designed for people with disabilities? For example they are large enough for a wheelchair to turn easily, space under the bed for a lift.",
      },
      {
        "name": "Roll-in Shower",
        "value": this.place['int_hotel_shower'],
        "id": "7",
        "display": false,
        "description": "Does the hotel provide rooms with a step free shower option?",
      }
    ];

    if (poi_id == undefined || poi_id == null) {
      poi_id = this.place['place_id'];
    }
    if (this.place['category_id'] == 1) {
      local[3].description = "Is there step free or elevator access to both the hotel rooms and restaurant?";
      local[7].display = true;
      local[8].display = true;
    }
    else if (this.place['category_id'] == 2) {
      local[3].description = "Are tables available without having to go up or down steps?"
    }
    else if (this.place['category_id'] == 3) {
      local[3].description = "Are aisles wide enough to allow easy access through shop in order to browse and pay?";
    }
    else if (this.place['category_id'] == 4) {
      local[3].description = "Can you access the main attraction via a step free route?";
    }
    else {
      local[3].description = "Is everything you expect from a venue accessible either by elevator or otherwise? For example a bar with steps up but table service is available.";

    }
    this.placeCriteria = local;

    console.log(this.placeCriteria);
  }

}
